from django.shortcuts import get_object_or_404, render
from django.core.urlresolvers import reverse
from django.template import loader
from django.views import generic
from .models import Minion

import salt.client
import salt.config
import salt.loader
# Create your views here.
def index(request):
	local = salt.client.LocalClient()
	listaMinions=local.cmd('*', 'test.ping', '')
	context = {'listaMinions':listaMinions}    	
	return render(request, 'indice.html', context)
#ESTE def ES EL QUE USABA CUANDO UTILIZABA LA TABLA DE ADMIN, PERO LO DEJE DE USAR
#def detalle(request, minion_id):
#	minion = get_object_or_404(Minion, pk=minion_id)
#	__opts__ = salt.config.minion_config('/etc/salt/minion')
#	__grains__ = salt.loader.grains(__opts__)
#	__opts__['grains'] = __grains__
#	__utils__ = salt.loader.utils(__opts__)
#	__salt__ = salt.loader.minion_mods(__opts__, utils=__utils__)
#	x=__salt__['service.status']('nginx')
#	quienSoy=__salt__['cmd.run']('uname -n')
#	local = salt.client.LocalClient()
#	comando2=local.cmd('minionF', 'cmd.run', ['df -h /home'])
#	context = {'minion': minion, 'x':x, 'quienSoy':quienSoy, 'comando2':comando2}
 #   	return render(request, 'detalle.html', context)

#AQUI QUIERO QUE RECIBA EL NOMBRE DEL MINION QUE HE PULSADO EN LA LISTA DE MINIONS DISPONIBLES Y EJECUTAR UN COMANDO SOBRE ÉL
#PERO NO LLEGA BIEN EL NOMBRE
def detalle2(request, nombre):
	print nombre
	context = {'nombre':nombre}
	return render(request, 'detalle2.html', context)
