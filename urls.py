from django.conf.urls import url

from . import views

app_name = 'directorio'

urlpatterns=[
	#VISTA INDICE, DONDE SE MUESTRA LA LINEA DE LOS DETERMINADOS MINIONS DISPONIBLES (http://127.0.0.1:8000/directorio/)
	url(r'^$', views.index, name='index'),
	#ESTA ES LA URL QUE UTILIZABA CUANDO USABA LA TABLA DE ADMIN, AHORA MISMO NO LA USO
		url(r'^(?P<minion_id>[0-9]+)/$', views.detalle, name='detalle'),
	
	#CON ESTA URL SI ME DEJA ENVIAR UN NUMERO A LA VISTA DE DETALLE2
		#url(r'^directorio/(\d{1,2})/$', views.detalle2, name='detalle2'),
	
	#CON ESTA URL ES CON LA QUE QUIERO QUE RECIBA EL NOMBRE EN LA VISTA DETALLE2, PERO IMPOSIBLE RECIBIR UNA CADENA, NOMBRE DE MINION(http://127.0.0.1:8000/directorio/directorio/nombre)
	url(r'^directorio/([a-z])/$', views.detalle2, name='detalle2'),
]
